# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalendar
pkgver=0_git20211027
pkgrel=0
_commit="83302300a6263278c4cca35e2eaeb0a99ffb1b4b"
pkgdesc="A calendar application using Akonadi to sync with external services (NextCloud, GMail, ...)"
# armhf blocked by qt5-qtdeclarative
# mips64, s390x and riscv64 blocked by polkit -> akonadi
# ppc64le blocked by kaccounts-integration -> akonadi
arch="all !armhf !mips64 !s390x !riscv64 !ppc64le"
url="https://invent.kde.org/pim/kalendar"
license="GPL-3.0-or-later AND BSD-2-Clause"
depends="
	kdepim-runtime
	kirigami2
	qt5-qtlocation
	"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	eventviews-dev
	extra-cmake-modules
	kcalendarcore-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami2-dev
	kitemmodels-dev
	kpackage-dev
	kpeople-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	"
source="https://invent.kde.org/pim/kalendar/-/archive/$_commit/kalendar-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f41599cb613214d1ce3f9bb03d9fef8fe853c5f4163bc8bafa2c4c47177c9a8f01f16cdd0c54f9cbba536f88621a777560b0600a321344d1b0f70212fb61380d  kalendar-83302300a6263278c4cca35e2eaeb0a99ffb1b4b.tar.gz
"
