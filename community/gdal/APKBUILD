# Contributor: Trevor R.H. Clarke <trevor@notcows.com>
# Maintainer: Holger Jaekel <holger.jaekel@gmx.de>
pkgname=gdal
pkgver=3.3.3
pkgrel=1
pkgdesc="A translator library for raster and vector geospatial data formats"
url="https://gdal.org/"
arch="all !s390x !mips !mips64" # limited by geos
license="MIT"
depends_dev="gdal"
makedepends="
	armadillo-dev
	cfitsio-dev
	charls-dev
	chrpath
	curl-dev
	expat-dev
	freexl-dev
	geos-dev
	giflib-dev
	hdf5-dev
	json-c-dev
	libdap-dev
	libheif-dev
	libkml-dev
	libpng-dev
	libwebp-dev
	libxml2-dev
	linux-headers
	mariadb-dev
	ogdi-dev
	openexr-dev
	openjpeg-dev
	poppler-dev
	libpq-dev
	proj-dev
	py3-numpy
	py3-numpy-dev
	python3-dev
	qhull-dev
	libspatialite-dev
	sqlite-dev
	swig
	tiff-dev
	unixodbc-dev
	xerces-c-dev
	zlib-dev
	zstd-dev
	"
checkdepends="
	pytest
	"
subpackages="
	$pkgname-static
	$pkgname-dev
	py3-$pkgname:_py3
	$pkgname-tools
	"

source="
	$pkgname-$pkgver.tar.gz::https://github.com/OSGeo/gdal/archive/v$pkgver.tar.gz
	10-atoll.patch
	20-userfaultfd-detection.patch
	"

# Optional dependency netcdf-dev is not available on mips and s390x
case "$CARCH" in
	mips*|s390x) ;;
	*) makedepends="$makedepends netcdf-dev" ;;
esac

# Optional dependency librasterlite2-dev is only available on x86 and x86_64
_with_librasterlite2="no"
case "$CARCH" in
	x86|x86_64)
		makedepends="$makedepends librasterlite2-dev"
		_with_librasterlite2="yes"
		;;
esac

# Optional dependency java-jdk is not available on mips and riscv64
_with_java=""
case "$CARCH" in
	mips*|riscv64) ;;
	*)
		makedepends="$makedepends apache-ant java-jdk"
		subpackages="$subpackages java-$pkgname:_java"
		_with_java="--with-java=/usr/lib/jvm/default-jvm"
		;;
esac

build() {
	cd "$builddir"/gdal

	CPPFLAGS="$CPPFLAGS -I/usr/include/mysql/server -I/usr/include/tirpc" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-cfitsio=/usr \
		--with-armadillo=yes \
		--with-spatialite=yes \
		--with-rasterlite2=$_with_librasterlite2 \
		--with-cpp14 \
		--with-dods-root=/usr \
		--with-poppler \
		--with-mysql \
		$_with_java
	make

	cd "$builddir/gdal/swig/python"
	python3 setup.py build

	if [ -z "$_with_java" ]; then
		cd "$builddir/gdal/swig/java"
		make
	fi
}

package() {
	cd "$builddir"/gdal

	make DESTDIR="$pkgdir" install
	chmod -x "$pkgdir"/usr/include/*.h

	cd "$pkgdir/usr/bin"
	mv gdal-config ..
	rm -rf "$pkgdir/usr/bin/"*
	mv ../gdal-config .
}

_py3() {
	pkgdesc="$pkgdesc (Python3 bindings)"
	depends="py3-numpy"

	mkdir -p "$subpkgdir"/usr/lib/
	cd "$builddir"/gdal/swig/python
	python3 setup.py install --prefix=/usr --root="$subpkgdir"
	rm -rf "$subpkgdir/usr/bin"
}

_java() {
	pkgdesc="$pkgdesc (Java bindings)"
	cd "$builddir"/gdal/swig/java
	make

	mkdir -p "$subpkgdir/usr/lib" "$subpkgdir/usr/share/java/"
	chrpath -d .libs/*.so*
	mv .libs/*.so* "$subpkgdir/usr/lib"
	cp gdal.jar "$subpkgdir/usr/share/java/gdal-$pkgver.jar"
	cd "$subpkgdir/usr/share/java/"
	ln -s gdal-$pkgver.jar gdal.jar
}

tools() {
	pkgdesc="$pkgdesc (command line utilities)"
	depends="py3-$pkgname"

	cd "$builddir"/gdal/swig/python/gdal-utils
	chmod a+x scripts/*

	install -d "$subpkgdir/usr/bin"
	install -m755 scripts/*.py "$subpkgdir/usr/bin/"

	cd "$builddir"/gdal
	make DESTDIR="$subpkgdir" install
	rm -rf "$subpkgdir/usr/include" "$subpkgdir/usr/lib" "$subpkgdir/usr/share" "$subpkgdir/usr/bin/gdal-config"
}

check() {
	# TODO: https://trac.osgeo.org/gdal/wiki/TestingNotes

	cd "$builddir"/gdal
	apps/gdal-config --version | grep "$pkgver"

	# confirms MBTiles support
	apps/gdal_translate --formats | grep "MBTiles -raster,vector- (rw+v): MBTiles"

	# confirms PostgreSQL/PostGIS support
	apps/ogr2ogr --formats | grep "PostgreSQL -vector- (rw+): PostgreSQL/PostGIS"

}
sha512sums="
ec6ea09ff4c1c2674ee1f76e6ea77b250fdbc9beba158d43c4ee738fc16a56a1b22b0517a6c89674074ad742a954851d7466c67b7e13f4a330e9b1e7474dcd6a  gdal-3.3.3.tar.gz
14d080b80a5e499840e7b356acd0a7f6afaec6c65a10d9df864011d89e93310bd3dac22e59ca7c0cd93e6fd2277f85a71920a91ed5afdb352c18e953b2b4783b  10-atoll.patch
4741d6f9f2aaaa80c1ad590073d0b960d4389b8908682de224b476136cc5dd898375ca67498568fac4537a2c23f00e42998618a4a9121ac6d9f82dd61eda15d8  20-userfaultfd-detection.patch
"
