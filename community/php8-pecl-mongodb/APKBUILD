# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php8-pecl-mongodb
_extname=mongodb
pkgver=1.11.0
pkgrel=0
pkgdesc="PHP 8 MongoDB driver - PECL"
url="https://pecl.php.net/package/mongodb"
arch="all"
license="Apache-2.0"
depends="php8-common"
makedepends="cyrus-sasl-dev icu-dev openssl1.1-compat-dev php8-dev snappy-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir"/$_extname-$pkgver
provides="php8-mongodb=$pkgver-r$pkgrel" # for backward compatibility
replaces="php8-mongodb" # for backward compatibility

build() {
	phpize8
	./configure --prefix=/usr \
		--with-php-config=/usr/bin/php-config8
	make
}

check() {
	# tests requires additional dependencies (vagrant)
	php8 -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/php8/conf.d
	install -d $_confdir
	echo "extension=$_extname.so" > $_confdir/$_extname.ini
}

sha512sums="
1b55688262b28bc15f6746df68e359eb592235bc0b61769d702329b5eaf4a074bbff8b16cb56d78fb1e1a9ffb0f52fcf5d0fc78eeb743705fc31801f97e3cc05  php-pecl-mongodb-1.11.0.tgz
"
