# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-jsonschema
pkgver=4.1.2
pkgrel=0
pkgdesc="An implementation of JSON Schema validation for Python"
url="https://github.com/Julian/jsonschema"
arch="noarch"
license="MIT"
depends="python3 py3-pyrsistent py3-attrs py3-six"
makedepends="py3-setuptools py3-setuptools_scm pyproject2setuppy"
checkdepends="py3-twisted py3-pytest py3-pip py3-tox"
source="https://files.pythonhosted.org/packages/source/j/jsonschema/jsonschema-$pkgver.tar.gz"
builddir="$srcdir/jsonschema-$pkgver"

replaces="py-jsonschema" # Backwards compatibility
provides="py-jsonschema=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 -m pyproject2setuppy.main build
}

check() {
        PYTHONPATH="$PWD/build/lib" py.test-3 -v \
                --deselect jsonschema/tests/test_cli.py::TestCLIIntegration::test_license
}

package() {
	python3 -m pyproject2setuppy.main install --prefix=/usr --root="$pkgdir"

	# Add version suffix to executable files.
	local path; for path in "$pkgdir"/usr/bin/*; do
		mv "$path" "$path"-3
	done

	ln -s jsonschema-3 "$pkgdir"/usr/bin/jsonschema
}

sha512sums="
5ab43602cfc2dfe07a8c733f01915b4961ee88ebb37723a7a3a6ee4793ddae4b4638abe77ac9d192158ae7634aacee0c58fa2959288ea0f83e3403d5f8b9f185  jsonschema-4.1.2.tar.gz
"
